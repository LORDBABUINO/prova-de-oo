package model;

public class Usuario {
	
	//atributos
	
	private String nome;
	private String email;
	private String senha;
	private Proposta umaProposta;
	
	//métodos
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public void criarProposta(String nome, String descricao){
		umaProposta = new Proposta(nome,this.nome, descricao);
	}
	public void votar(){
		umaProposta.setVoto(nome);
	}
	public void comentar(String comentario){
		umaProposta.setComentario(comentario);
	}
	public void editarProposta(Proposta umaProposta, String descricao){
		if(this.nome.equals(umaProposta.getDono())){
			umaProposta = new Proposta(umaProposta.getNome(),this.nome, descricao);
		}
	}
}
