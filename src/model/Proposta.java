package model;

import java.util.*;

public class Proposta {
	
	//atributos
	
	private String nome;
	private String dono;
	private String descricao;
	private ArrayList<String> comentarios;
	private ArrayList<String> Votos;
	
	//Construtor
	
	public Proposta (String nome,String dono, String descricao){
		this.nome = nome;
		this.dono = dono;
		this.descricao = descricao;
	}

	//Metodos
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDono() {
		return dono;
	}

	public void setDono(String dono) {
		this.dono = dono;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public ArrayList<String> getComentarios() {
		return comentarios;
	}

	public void setComentario(String comentario) {
		comentarios.add(comentario);
	}

	public ArrayList<String> getVotos() {
		return Votos;
	}

	public void setVoto(String voto) {
		Votos.add(voto);
	}
}