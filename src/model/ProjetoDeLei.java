package model;

public class ProjetoDeLei extends Proposta{
	
	//atributos
	
	private String numero;
	
	//Construtor
	
	ProjetoDeLei(String nome, String dono, String descricao, String numero){
		super(nome, dono, descricao);
		this.numero = numero;
	}
	
	//Métodos
	
	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}
	

}
