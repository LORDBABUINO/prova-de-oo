package model;

public class Denuncia extends Proposta{
	
	//atributos
	
	private String alvo;
	
	//Construtor
	
	Denuncia(String nome, String dono, String descricao, String alvo){
		super(nome, dono, descricao);
		this.alvo = alvo;
	}

	//métodos
	
	public String getAlvo() {
		return alvo;
	}

	public void setAlvo(String alvo) {
		this.alvo = alvo;
	}

}
